#!/home/vidzor/Projects/codesync/envVS/bin/python

import os
from pathlib import Path

import utils
import upsync_utils
import downsync_utils


def prompt_to_proceed():
	while True:
		cin = input(" Proceed (hit RETURN for YES, 'x' for NO ): ").strip().lower()
		if cin == 'x': return False
		if len(cin) == 0: return True


def upsync_changes( git_path: Path, changes_matcher ):
	print("\n--- Consuming the output from 'git status' --- ")
	changes = upsync_utils.get_changes(git_path, changes_matcher)
	if changes is None: print(' There are no changes. '); return
	utils.print_changes(changes)

	print('\n--- Creating the changes package zip file ---')
	if not prompt_to_proceed(): return
	package_dir_path = upsync_utils.create_changes_package(changes, git_path)
	if package_dir_path is None: return

	print('\n--- Uploading the package zip --- ')
	if not prompt_to_proceed(): return
	package_zip_path = Path(f'{package_dir_path}.zip')
	upsync_utils.upload_code_changes_package(package_zip_path)

	print('\n--- Cleanup --- ')
	if not prompt_to_proceed(): return
	upsync_utils.cleanup(package_dir_path, package_zip_path)


def downsync_changes( git_path: Path ):
	print('\n--- Fetching the list of changes from the server --- ')
	package_names_list = downsync_utils.list_code_changes_packages( git_path )
	if len(package_names_list) == 0: print(' There are no changes. '); return
	package_names_list = downsync_utils.sort_packages_list( package_names_list )

	package_name = downsync_utils.prompt_which_package_to_load( package_names_list )
	if package_name is None: print(' Exiting... '); return

	print(f'\n--- Downloading: {package_name} --- ')
	package_zip_path = downsync_utils.download_code_changes_package( package_name )
	if not package_zip_path.exists(): print(' Package file missing. '); return

	print(' \n--- Extracting the package --- ')
	if not prompt_to_proceed(): return
	changes, package_extracted_dir = downsync_utils.extract_package_data( package_zip_path )

	print(' \n--- Applying changes --- ')
	utils.print_changes( changes )
	if not prompt_to_proceed(): return
	downsync_utils.apply_changes( changes, package_extracted_dir, git_path )

	print(' \n--- Cleanup --- ')
	if not prompt_to_proceed(): return
	downsync_utils.cleanup( package_extracted_dir, package_zip_path )


def main():
	git_path = Path( os.getcwd() )
	changes_matcher = dict( new='N', modify='M', delete='D' )

	direction = utils.get_CL_arguments()
	if direction is None: print(" Invalid argument, must be 'up' or 'down' "); return

	if direction == 'up':   upsync_changes( git_path, changes_matcher )
	if direction == 'down': downsync_changes( git_path )


if __name__=='__main__': main()