
import os
import re
import subprocess
import json
from datetime import datetime
from pathlib import Path
import shutil

import utils

def get_changes( git_path, changes_matcher ):
	"""
	Parses the "git status" data
	:param git_path: the path of the project repo
	:param changes_matcher: what types of changes are being looked for
	:return: changes dictionary with grouped list of file names/paths
	"""
	changes = { key: [] for key in list(changes_matcher.keys()) }

	# change dir to the repo:
	cwd = os.getcwd()
	os.chdir(str(git_path))

	# catch "git status" output:
	proc = subprocess.Popen(['git', 'status', '--porcelain'], stdout=subprocess.PIPE)
	changes_raw = proc.stdout.read().decode('utf-8').strip()
	if len(changes_raw) == 0: return None

	# catch the listing of all files output:
	all_filepaths_list = utils.list_all_files( git_path )

	# change dir back to where it was:
	os.chdir(cwd)

	# replace "??" with "N", to simplify REGEX expression for it.
	# ("??" is the default git output for new untracked files)
	# also, break the changes into list of lines:
	changes_raw_lines = changes_raw.replace('??', 'N').strip().split('\n')

	for change_raw_line in changes_raw_lines:
		change_raw_line = change_raw_line.strip()
		for change_type, matcher in changes_matcher.items():
			file_match = re.findall(rf'{matcher}\s+(.+)', change_raw_line)
			if not file_match: continue
			filename = file_match[0]
			if matcher == 'D':
				# can not get the full path of a deleted file
				changes[change_type].append(filename)
			else:
				filepaths = utils.get_full_filepath(filename, all_filepaths_list)
				for fp in filepaths:
					changes[change_type].append( fp.as_posix() )

	return changes


def edit_changes_manually( changes_json_path: Path ):
	if not changes_json_path.exists(): print(f' Changes JSON file does not exist. '); return False
	while True:
		cin = input(" Edit the changes JSON file manually (hit RETURN for YES, 'x' for NO): ").strip().lower()
		if cin == 'x': do_the_edit = False; break
		if len(cin) == 0: do_the_edit = True; break
	if do_the_edit:
		subprocess.call([ 'code', str(changes_json_path) ])
		while True: input(" Done editing (hit RETURN): "); break
		with open(changes_json_path, 'r') as changes_json:
			changes = json.load(changes_json)
		utils.print_changes( changes )


def create_changes_package( changes, git_path: Path ):
	"""
	Creates the package structure, serializes tha changes objecst and zippes it all together
	:param changes: changes dictionary with grouped list of file names/paths
	:param git_path: the path of the project repo
	:return: the location of the created package directory
	"""
	# init tmp package dir:
	timestamp = utils.timestamp_format.format(datetime.now())
	package_dir_path = Path( utils.changes_dir, f'{git_path.parts[-1]}__changes-{timestamp}' )
	if not package_dir_path.exists(): os.makedirs( package_dir_path )
	
	# save changes objects to a JSON file:
	changes_json_path = Path( package_dir_path, utils.changes_json_name )
	with open( changes_json_path, 'w' ) as changes_json:
		json.dump( changes, changes_json )
	# prompt the user to edit the changes manually:
	edit_changes_manually( changes_json_path )
	
	# copy over the modified and new files:
	for change_type, filepaths in changes.items():
		if change_type == 'delete': continue
		for filepath in filepaths:
			file_src_path = Path( git_path, filepath )
			file_dst_path = Path( package_dir_path, filepath )
			os.makedirs( file_dst_path.parent, exist_ok=True )
			shutil.copy( file_src_path, file_dst_path )

	# create a package:
	shutil.make_archive( package_dir_path, 'zip', package_dir_path )

	return package_dir_path


def upload_code_changes_package( package_zip_path ):
	"""
	Copies the package zip over to the server
	:param package_zip_path: path to the zip package
	"""
	local_src_location = str(package_zip_path)
	server_location = f"{utils.server_base}:{utils.server_changes_dir}"
	subprocess.call([ 'scp', local_src_location, server_location ])


def cleanup( package_dir_path, package_zip_path ):
	"""
	Basic cleanup, deletes the package dir and zip
	:param package_dir_path: path to the extracted package dir
	:param package_zip_path: path to the package zip
	"""
	if package_dir_path.exists(): shutil.rmtree( package_dir_path )
	if package_zip_path.exists(): os.remove( package_zip_path )