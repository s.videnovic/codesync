
import os
import re
import subprocess
from pathlib import Path
from datetime import datetime
import shutil
import json
import numpy as np

import utils


def list_code_changes_packages( git_path: Path ):
	"""
	Get the listing of all project changes that are online
	:param git_path: the path of the project repo
	:return: list of change package names
	"""
	proc = subprocess.Popen(
		[ 'ssh', utils.server_base, 'ls', utils.server_changes_dir ],
		stdout=subprocess.PIPE
	)
	packages_list_raw = proc.stdout.read().decode('utf-8').strip()
	if len(packages_list_raw) == 0: return []

	packages_list = re.sub( r'\s+', ' ', packages_list_raw ).split(' ')

	# filter out unrelated files:
	project_name = git_path.parts[-1]
	packages_list = [
		pkg for pkg in packages_list
		if re.findall( project_name, pkg ) and re.findall( r'[.]zip', pkg )
	]
	if len(packages_list) == 0: return []

	return packages_list


def sort_packages_list( package_names_list ):
	"""
	Sort package names by dates (ascending)
	:param package_names_list: list of package names
	:return: sorted list of package names
	"""
	dates_list_raw = [ utils.timestamp_regex.findall( pkg )[0] for pkg in package_names_list ]

	timestamp_format = utils.timestamp_format[ 2:-1 ]
	dates_list = [ datetime.strptime( dt, timestamp_format ) for dt in dates_list_raw ]

	sorted_date_indices = np.argsort( dates_list )
	package_names_list = (np.array(package_names_list)[ sorted_date_indices ]).tolist()

	return package_names_list


def prompt_which_package_to_load( package_names_list ):
	"""
	Convenience function for prompting the user to select a package
	Valid inputs:
		>> 'x' - exits the loop (and the general script)
		>> '' - (empty, just hit RETURN) selects the latest package
		>> a number - should be in range [1,n] where n is the length of the package list displayed
	:param package_names_list: a list of changes packages that are online (related to the project)
	"""
	print('\n Choose the changes package: ')
	for p,pkg in enumerate(package_names_list): print(f'    {p+1}. {pkg} ')
	while True:
		cin = input(" Enter a number (or hit RETURN for the latest, 'x' for exit): ").strip().lower()
		if len(cin) == 0: return package_names_list[-1]
		if cin == 'x': return None
		try: number = int(cin)
		except ValueError: number = 0
		if 1 <= number <= len(package_names_list): return package_names_list[ number-1 ]


def download_code_changes_package( package_name ):
	"""
	Downloads the given package zip from the server
	:param package_name: the name of the package (without extension)
	:return: the location of the downloaded zip package on the local machine
	"""
	server_src_location = f'{utils.server_base}:{utils.server_changes_dir}/{package_name}'
	if not utils.changes_dir.exists(): os.makedirs( utils.changes_dir )
	local_dst_location = Path( utils.changes_dir, package_name )
	subprocess.call([ 'scp', server_src_location, str(local_dst_location) ])
	return local_dst_location


def extract_package_data( package_zip_path ):
	"""
	Extracts the package zip and deserializes changes object
	:param package_zip_path: path to the pzkage zip file
	:return: changes dictionary with grouped list of file names/paths
	"""
	parts = package_zip_path.parts
	package_extracted_dir = Path( *parts[:-1], parts[-1].replace('.zip','') )
	shutil.unpack_archive( package_zip_path, package_extracted_dir )

	changes_json_path = Path( package_extracted_dir, utils.changes_json_name )
	with open( changes_json_path, 'r' ) as changes_json:
		changes = json.load( changes_json )

	return changes, package_extracted_dir


def apply_changes( changes, package_extracted_dir, git_path ):
	"""
	Applies the changes to the given git repository
	:param changes: changes dictionary with grouped list of file names/paths
	:param package_extracted_dir: location of the extracted package
	:param git_path: the path of the project repo
	"""
	# catch the listing of all files output:
	all_filepaths_list = utils.list_all_files( git_path )
	# apply changes:
	for change_type, filepaths in changes.items():

		if change_type == 'new' or change_type == 'modify':
			for filepath in filepaths:
				file_src_path = Path( package_extracted_dir, filepath )
				file_dst_path = Path( git_path, filepath )
				if not file_dst_path.parent.exists():
					os.makedirs( file_dst_path.parent )
				shutil.copy( file_src_path, file_dst_path )

		elif change_type == 'delete':
			for filename in filepaths:
				file2remove_paths = utils.get_full_filepath( filename, all_filepaths_list )
				for file2remove_path in file2remove_paths:
					if file2remove_path.exists(): os.remove( file2remove_path )

		else: continue


def cleanup( package_extracted_dir, package_zip_path ):
	"""
	Basic cleanup, deletes the package dir and zip
	:param package_extracted_dir: path to the extracted package dir
	:param package_zip_path: path to the package zip
	"""
	if package_extracted_dir.exists(): shutil.rmtree( package_extracted_dir )
	if package_zip_path.exists(): os.remove( package_zip_path )


