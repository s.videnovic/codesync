# Code Sync

This is a small utility that works as an extremely simplified GIT. It works only on Linux OS



### Install instructions

```bash
cd /home/vidzor/Projects/codesync
# Create & activate python venv, then install deps:
python3 -m venv envVS
source envVS/bin/activate
pip install -r requirements.txt
# open up `codesync.py` and edit the first line:
#!/home/vidzor/Projects/codesync/envVS/bin/python
# make `codesync.py` executable:
sudo chmod +x codesync.py
# create a soft link for it:
sudo ln -s /home/vidzor/Projects/codesync/codesync.py /usr/local/bin/codesync
```



### How to use

```bash
# go to the projects directory:
cd /home/vidzor/Projects/hacker_news
codesync up   # uploads changes (uses git status)
codesync down # downloads & applies changes
```

