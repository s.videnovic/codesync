
import os
import re
import subprocess
from pathlib import Path


script_wd = Path(os.path.dirname(os.path.abspath(__file__)))
changes_dir = Path( script_wd, 'changes')
changes_json_name = '_changes_.json'
server_base = "root@134.122.117.227"
server_changes_dir = "/home/vidzor/data/git_code_changes"
timestamp_format = "{:%d.%m.%Y_%H:%M:%S}"
timestamp_regex = re.compile( r'(\d+[.]\d+[.]\d+_\d+[:]\d+[:]\d+)' )


def list_all_files( git_path: Path ):
	"""
	Will recursively look through the given git_path
	anf return a list of all files (absolute paths), excluding the node_modules directory
	:param git_path: the path of the project repo
	:return: list of all files (absolute paths), excluding the node_modules directory
	"""
	# change dir to the repo:
	cwd = os.getcwd()
	if str(cwd) != str(git_path): os.chdir(str(git_path))
	# catch the listing of all files output:
	proc = subprocess.Popen(
		['find', '.', '-type', 'f', '-not', '-path', "./node_modules/**"],
		stdout=subprocess.PIPE
	)
	all_filepaths_list = proc.stdout.read().decode('utf-8').strip().split('\n')
	# change dir back to where it was:
	if str(cwd) != str(git_path): os.chdir(cwd)
	return all_filepaths_list


def get_full_filepath( filename, all_filepaths_list ):
	"""
	Lookup the full path of the given filename
	:param filename: just the filename (or a partial path)
	:param all_filepaths_list: list of all (absolute) filepaths to look through
	:return: full filepaths for the given filename (can be more than 1, if it matches)
	"""
	filepaths = []
	for filepath in all_filepaths_list:
		match = re.findall( filename, str(filepath) )
		if match: filepaths.append( Path(filepath) )
	return filepaths


def print_changes( changes ):
	"""
	Convenience function to show all changes to the user (to check before going ahead)
	:param changes: changes dictionary with grouped list of file names/paths
	"""
	print('-'*100)
	add_newline = False
	for change_type, filepaths_list in changes.items():
		if add_newline: print()
		print(f' {change_type.upper()} files: ')
		for fp in filepaths_list: print(' '*4 + fp)
		add_newline = True
	print('-' * 100)


def get_CL_arguments():
	"""
	Simple CL arguments parser, in this case meant to only get the sync direction
	:return: sync direction ('up' or 'down')
	"""
	if len( os.sys.argv ) < 2: return None
	direction = os.sys.argv[1].lower()
	if direction != 'up' and direction != 'down': return None
	return direction


